# README #

### Setup ###

!! RECOMENDO O USO DE XAMPP PARA RODAR O BANCO !!
https://www.apachefriends.org/pt_br/index.html

O projeto em Laravel está usando o port 3308 para acessar o banco de dados.
Caso prefira, é possível alterar o port do projeto no arquivo .env.

O projeto mobile pode ser rodado através do software do PhoneGap (http://phonegap.com/getstarted/).
Após a instalação do software, basta fazer o apontamento para a pasta Mobile e iniciar o server através do software.

Como PhoneGap não trabalha com programação server-side, foi necessário utilizar de arquivos externos, em PHP, para que fosse possível fazer a comunicação com o BD.
Tais arquivo se encontram dentro da pasta db. Tal pasta deve ser colocada na raiz do localhost.

Realizei a exportação do banco, bastando fazer a importação dele no phpmyadmin, o arquivo em questão é painel_db.sql.