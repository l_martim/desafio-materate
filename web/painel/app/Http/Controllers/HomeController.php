<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mobile;
use App\Deleted;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data["data"] = Mobile::all();
        return view('site.painel', $data);
    }

    public function editar($id)
    {
        $result = Mobile::find($id);

        if($result){
            $data['data'] = $result;

            return view('site.editar', $data);
        }else{
            return redirect()->back()->with("error", "Registro não encontrado.");
        }
    }

    public function atualizar(Request $request, $id){
        Mobile::find($id)->update($request->all());
        return redirect("/")->with("success", "Registro gravado com sucesso.");
    }

    // public function putUpdate(Request $request, $id)
    // {
    //     $rules = [
    //         "email" => "required",
    //     ];
    //
    //     $this->validate($request, $rules);
    //     $data = $request->all();
    //
    //     CadastroEmails::find($id)->update($data);
    //     return redirect("manager/emails")->with("success", "Registro gravado com sucesso.");
    // }

    public function delete($id)
    {
        $result = Mobile::find($id);
        $data['email'] = $result->email;
        Deleted::create($data);
        Mobile::find($id)->delete();
        return redirect("/")->with("success", "Registro deletado com sucesso.");
    }

    public function deletados(){
        $data["data"] = Deleted::all();
        return view('site.deletados', $data);
    }
}
