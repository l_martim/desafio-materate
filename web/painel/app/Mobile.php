<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    protected $table = "mobile_users";
    protected $fillable = [
        "email",
        "password"
    ];
}
