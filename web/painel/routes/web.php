<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/editar/{id}', 'HomeController@editar');
Route::put('/editar/atualizar/{id}', 'HomeController@atualizar');
Route::get('/deletar/{id}', 'HomeController@delete');
Route::get('/deletados', 'HomeController@deletados');
