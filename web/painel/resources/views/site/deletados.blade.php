@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="card-body box-top">
                    <a href="/" class="btn btn-success">Página Principal</a> <a href="/deletados" class="btn btn-warning">Usuário Deletados</a>
                </div>
                <table class="table_users">
                    <thead>
                        <tr>
                            <th class="table_id">ID</th>
                            <th class="table_email">E-mail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                            <tr id="{{$row->id}}">
                                <td>
                                    {{$row->id}}
                                </td>
                                <td>
                                    {{$row->email}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
