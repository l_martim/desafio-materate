@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="card-body">
                    {!! Form::open(["url" => url("/editar/atualizar/".$data->id), "method" => "PUT" , "files" => true ]) !!}
                        <div class="form-group">
                            {!! Form::label("email", "E-mail") !!}<br>
                            {!! Form::text("email", $data->email) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label("password", 'senha') !!}<br>
                            {!! Form::text("password", $data->password) !!}
                        </div>
                        <button type="submit" class="btn btn-success">Enviar</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
